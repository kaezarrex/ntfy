// ntfy publishes notifications using https://ntfy.sh.
//
// This is a work in progress!
package ntfy

import (
	"net/http"
	"strings"
)

const (
	PriorityUrgent  = "urgent"
	PriorityHigh    = "high"
	PriorityDefault = "default"
	PriorityLow     = "low"
	PriorityMin     = "min"
)

type Client struct {
	topic string
}

func New(topic string) Client {
	return Client{
		topic: topic,
	}
}

// Publish publishes a message.
func (c *Client) Publish(msg string, opts ...PublishOption) {
	req, _ := http.NewRequest("POST", "https://ntfy.sh/"+c.topic, strings.NewReader(msg))
	for _, opt := range opts {
		opt(req)
	}
	http.DefaultClient.Do(req)
}

// Urgent publishes a message with urgent priority.
func (c *Client) Urgent(msg string, opts ...PublishOption) {
	c.Publish(msg, append(opts, Priority(PriorityUrgent))...)
}

// High publishes a message with high priority.
func (c *Client) High(msg string, opts ...PublishOption) {
	c.Publish(msg, append(opts, Priority(PriorityHigh))...)
}

// Default publishes a message with default priority.
func (c *Client) Default(msg string, opts ...PublishOption) {
	c.Publish(msg, append(opts, Priority(PriorityDefault))...)
}

// Low publishes a message with low priority.
func (c *Client) Low(msg string, opts ...PublishOption) {
	c.Publish(msg, append(opts, Priority(PriorityLow))...)
}

// Min publishes a message with min priority.
func (c *Client) Min(msg string, opts ...PublishOption) {
	c.Publish(msg, append(opts, Priority(PriorityMin))...)
}

// Icon sets a URL to use as a notification icon
func Icon(url string) PublishOption {
	return func(req *http.Request) {
		req.Header.Set("Icon", url)
	}
}

// Priority sets the message priority
func Priority(priority string) PublishOption {
	return func(req *http.Request) {
		req.Header.Set("Priority", priority)
	}
}

// Tags sets a list of tags that may or may not map to emojis
func Tags(tags []string) PublishOption {
	return func(req *http.Request) {
		req.Header.Set("Tags", strings.Join(tags, ","))
	}
}

// Title sets the message title
func Title(title string) PublishOption {
	return func(req *http.Request) {
		req.Header.Set("Title", title)
	}
}

type PublishOption func(*http.Request)
